<?php

$config_file = './config.json';
$GLOBALS['ldap_config'] = json_decode(file_get_contents($config_file), true);
$GLOBALS['allowed_auth'] = Array('PASSWORD_WARN', 'MFA_ENROLL', 'MFA_ENROLL_ACTIVATE', 'MFA_ENROLL_ACTIVATE', 'SUCCESS', 'MFA_CHALLENGE', 'MFA_REQUIRED');

/*
 * Function to create the okta API url
 *
 * USAGE
 *
 * $okta_url = oktaUrl();
 */
function oktaUrl() {
  $protocol = 'https';
  $okta_site = 'okta.com/api';
  $version = 'v1';
  $url = $protocol.
  "://".$GLOBALS['ldap_config']['okta_org'].
  ".".$okta_site.
  "/".$version;
  return $url;
}

/*
 * Returns the array of default headers when called with empty array of headers
 *
 * USAGE
 * $headers[] = 'X-Api-Key: test124';
 * $headers[] = 'X-User: testUser';
 * $okta_headers = oktaHeaders($headers);
 */
function oktaHeaders($headers = Array()){
  $head = array();
  $head[] = "Accept: application/json";  
  $head[] = "Content-Type: application/json";
  foreach($headers as $key => $val){
    $head[] = $val;
  }
  return $head;
}

/*
 * Creates the curl command and invkokes a call to OKTA API
 *
 * USAGE
 * $postvar = Array();
 * $api_action = 'authn'
 * $http_method = "POST";
 * $okta_url = oktaCurl($api_action, $postvar, $http_method);
 */
function oktaCurl($api_action, $postvar = Array(), $headers = NULL, $http_method = "POST") {
  $okta_url = oktaUrl() . "/" . $api_action;
  $ch = curl_init();
  if(is_null($headers)){
    $headers = oktaHeaders();
  } 

  curl_setopt($ch, CURLOPT_URL, $okta_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $http_method);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  // curl_setopt($ch, CURLOPT_VERBOSE, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  
  if($http_method == "POST"){
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postvar)); //PUT the variables
  }
  
  $server_output = curl_exec($ch);
  curl_close($ch);
  return $server_output;
}

/*
 * Authenticates the username and password using okta API
 *
 * USAGE
 *
 * oktaAuth($user, $pass) // Returns true or false
 */

function oktaAuth($user, $pass){
  $api_action = 'authn';
  $postvar['username'] = $user;
  $postvar['password'] = $pass;
  $postvar['relayState'] = gethostname();
  $postvar['options']['multiOptionalFactorEnroll'] = false;
  $postvar['options']['warnBeforePasswordExpired'] = true;
  $okta_response = json_decode(oktaCurl($api_action, $postvar), true);
  
  if(in_array($okta_response['status'], $GLOBALS['allowed_auth'])){
    return true;
  } else {
    return false;
  }
}

function oktaGroupMembership($user){
  // Preparing the headers for oktaCurl function
  $temp_header[] = "Authorization: SSWS " . $GLOBALS['ldap_config']['api_key'];
  $headers = oktaHeaders($temp_header);
  
  // Preparing to get the OKTA ID for the $user passed
  $api_action = 'users/' . $user;
  $user_response = json_decode(oktaCurl($api_action, NULL, $headers, "GET"), true); // obtaining the user information that contains the OKTA ID of the user
  
  // Preparing to get the group membership using the OKTA user ID
  $api_action = "users/" . $user_response['id'] . "/groups";
  $group_response = json_decode(oktaCurl($api_action, NULL, $headers, "GET"), true);
  foreach($group_response as $key => $val){
    $user_groups[] = $val['profile']['samAccountName'];
  }
  // Refining the $user_groups
  return array_filter($user_groups);
}

?>